import sys
from PyQt5 import QtCore, QtWidgets, QtGui

from WorkInputFile import InitFileWorker
from ElemInitFile import InitFileElements

import os
a = os.getcwd()

class MainFrame1DWOD(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.setMinimumSize(QtCore.QSize(640, 480))    
        self.setWindowTitle("WOD 1D RADIAL") 

        centralWidget = QtWidgets.QWidget(self)          
        self.setCentralWidget(centralWidget)   

        self.gridLayout = QtWidgets.QGridLayout(self)     
        centralWidget.setLayout(self.gridLayout)  


        self.makeStruct()

        self.statusBar().showMessage('Ready')

        exitAction = QtWidgets.QAction(QtGui.QIcon(a + '/gui/icons/exit24.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QtWidgets.qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)


        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)

        self.leftToolbar = QtWidgets.QToolBar(self)
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.leftToolbar)


    def makeStruct(self):
        self.makeLeftBlock()
        self.makeCenterBlock()

        self.blockSplitter = QtWidgets.QSplitter(self)
        self.blockSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.blockSplitter.addWidget(self.leftTabs)
        self.blockSplitter.addWidget(self.centerTabs)

        # set the initial scale: 4:1
        self.blockSplitter.setStretchFactor(2, 0)
        self.blockSplitter.setStretchFactor(1, 1)

        self.gridLayout.addWidget(self.blockSplitter, 0, 0)

    def makeLeftBlock(self):
        self.leftTabs = QtWidgets.QTabWidget()
        self.leftTab1 = QtWidgets.QWidget()

        self.leftTabs.addTab(self.leftTab1, "Settings")

        self.leftTabs.layout = QtWidgets.QVBoxLayout(self)
        self.leftTabs.setLayout(self.leftTabs.layout)
        print (self.size)

        width = self.frameGeometry().width()
        height = self.frameGeometry().height()

        self.leftTabs.resize(width * 0.5, height)



        self.leftTab1Setts = QtWidgets.QGroupBox('')
        self.leftTab1TableMain = QtWidgets.QGridLayout()

        rowInd = 0
        lblN = QtWidgets.QLabel('N')
        self.leftTab1TableMain.addWidget(lblN, rowInd, 0)
        self.sbN = QtWidgets.QSpinBox()
        self.sbN.setValue(10)
        self.sbN.setMaximum(10000)
        self.sbN.setMinimum(1)
        self.leftTab1TableMain.addWidget(self.sbN, rowInd, 1)
        rowInd = rowInd + 1

        lblrw = QtWidgets.QLabel('rw')
        self.leftTab1TableMain.addWidget(lblrw, rowInd, 0)
        self.sbrw = QtWidgets.QDoubleSpinBox()
        self.sbrw.setMinimum(1e-6)
        self.sbrw.setMaximum(0.1)
        self.sbrw.setSingleStep(1e-3)
        self.sbrw.setDecimals(5)
        self.sbrw.setValue(1e-3)
        self.leftTab1TableMain.addWidget(self.sbrw, rowInd, 1)
        rowInd = rowInd + 1

        lblr = QtWidgets.QLabel('R')
        self.leftTab1TableMain.addWidget(lblr, rowInd, 0)
        self.sbr = QtWidgets.QSpinBox()
        self.sbr.setMaximum(1000)
        self.sbr.setMinimum(1e-1)
        self.sbr.setValue(1)
        self.leftTab1TableMain.addWidget(self.sbr, rowInd, 1)
        rowInd = rowInd + 1

        lblUseCeff = QtWidgets.QLabel('UseCeff')
        self.leftTab1TableMain.addWidget(lblUseCeff, rowInd, 0)
        self.chUseCeff = QtWidgets.QCheckBox()
        self.leftTab1TableMain.addWidget(self.chUseCeff, rowInd, 1)
        rowInd = rowInd + 1

        lblInflow = QtWidgets.QLabel('Inflow')
        self.leftTab1TableMain.addWidget(lblInflow, rowInd, 0)
        self.chUseInflow = QtWidgets.QCheckBox()
        self.leftTab1TableMain.addWidget(self.chUseInflow, rowInd, 1)
        rowInd = rowInd + 1

        lblPwell = QtWidgets.QLabel('Press well')
        self.leftTab1TableMain.addWidget(lblPwell, rowInd, 0)
        self.sbPw = QtWidgets.QSpinBox()
        self.sbPw.setValue(0.0)
        self.sbPw.setMinimum(-1.0)
        self.sbPw.setMaximum(1.0)
        self.leftTab1TableMain.addWidget(self.sbPw, rowInd, 1)
        rowInd = rowInd + 1

        lblPwell = QtWidgets.QLabel('Press bound')
        self.leftTab1TableMain.addWidget(lblPwell, rowInd, 0)
        self.sbPg = QtWidgets.QSpinBox()
        self.sbPg.setValue(1.0)
        self.sbPg.setMinimum(-1.0)
        self.sbPg.setMaximum(1.0)
        self.leftTab1TableMain.addWidget(self.sbPg, rowInd, 1)
        rowInd = rowInd + 1

        lblPwell = QtWidgets.QLabel('Satur bound')
        self.leftTab1TableMain.addWidget(lblPwell, rowInd, 0)
        self.sbSg = QtWidgets.QSpinBox()
        self.sbSg.setValue(1.0)
        self.sbSg.setMinimum(-1.0)
        self.sbSg.setMaximum(1.0)
        self.leftTab1TableMain.addWidget(self.sbSg, rowInd, 1)
        rowInd = rowInd + 1

        lblUseCeff = QtWidgets.QLabel('Given q')
        self.leftTab1TableMain.addWidget(lblUseCeff, rowInd, 0)
        self.chGiven_q = QtWidgets.QCheckBox()
        self.leftTab1TableMain.addWidget(self.chGiven_q, rowInd, 1)
        rowInd = rowInd + 1

        lblPwell = QtWidgets.QLabel('q')
        self.leftTab1TableMain.addWidget(lblPwell, rowInd, 0)
        self.sbqw = QtWidgets.QSpinBox()
        self.sbqw.setValue(1.0)
        self.sbqw.setMinimum(-1.0)
        self.sbqw.setMaximum(1.0)
        self.leftTab1TableMain.addWidget(self.sbqw, rowInd, 1)
        rowInd = rowInd + 1


        lblCurant = QtWidgets.QLabel('Сurant')
        self.leftTab1TableMain.addWidget(lblCurant, rowInd, 0)
        self.sbCurant = QtWidgets.QDoubleSpinBox()
        self.sbCurant.setDecimals(4)
        self.sbCurant.setMaximum(1.0)
        self.sbCurant.setMinimum(0.000001)
        self.sbCurant.setValue(0.1)
        self.leftTab1TableMain.addWidget(self.sbCurant, rowInd, 1)
        rowInd = rowInd + 1

        self.btnSaveSettings = QtWidgets.QPushButton(u'Сохранить как ...', self)
        self.btnSaveSettings.setToolTip('Click to save settings')
        self.btnSaveSettings.clicked.connect(self.OnSaveButtonClick)
        self.leftTab1TableMain.addWidget(self.btnSaveSettings, rowInd, 0)

        self.btnOpenSettings = QtWidgets.QPushButton(u'Открыть', self)
        self.btnOpenSettings.setToolTip('Click to load settings')
        self.btnOpenSettings.clicked.connect(self.OnLoadButtonClicked)
        self.leftTab1TableMain.addWidget(self.btnOpenSettings, rowInd, 1)
        rowInd = rowInd + 1

        self.leftTab1Setts.setLayout(self.leftTab1TableMain)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.leftTab1Setts)
        self.leftTab1.setLayout(layout)
		

    def makeCenterBlock(self):
        self.centerTabs = QtWidgets.QTabWidget()
        tab1 = QtWidgets.QWidget()
        tab2 = QtWidgets.QWidget()
        tab3 = QtWidgets.QWidget()

        self.centerTabs.addTab(tab1, "Graphs")
        self.centerTabs.addTab(tab2, "Tables")

        layout = QtWidgets.QVBoxLayout(self)
        self.centerTabs.setLayout(layout)

    @QtCore.pyqtSlot()
    def OnSaveButtonClick(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(
                self, 
                u"Сохранить проект как...",
                "",
                "WOD1D Files (*.wod1d)", 
                options=options)
        fileName = fileName + '.wod1d'

        els = InitFileElements()
        w = InitFileWorker(self, els)
        w.SaveInitFile(fileName)

    @QtCore.pyqtSlot()
    def OnLoadButtonClicked(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(
                self,
                u"Открыть проект", 
                "",
                "WOD1D Files (*.wod1d)", 
                options=options)
        els = InitFileElements()
        w = InitFileWorker(self, els)
        w.ReadInitFile(fileName)
	
