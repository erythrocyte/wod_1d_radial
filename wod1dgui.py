import sys

import os
a = os.getcwd()

sys.path.insert(0, a + '/gui/')
sys.path.insert(0, a + '/gui/mainFrame/')
sys.path.insert(0, a + '/gui/icons/')
sys.path.insert(0, a + '/src/')
sys.path.insert(0, a + '/src/initFileWork/')
sys.path.insert(0, a + '/src/meshGenerator')
sys.path.insert(0, a + '/src/analycPress')

from PyQt5.QtWidgets import QApplication

import mainFrame
from mainFrame import MainFrame1DWOD

import makeAnalitycalCalculations
from makeAnalitycalCalculations import *


import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt

def main():
    [r, p] = GetAnalyticalPress(0.1, 0.01, 1, 1, 1000)
    [r1, p1] = GetAnalyticalPress(1, 0.01, 1, 1, 1000)

    fig, ax = plt.subplots()

    ax.plot(r, p, '', label = 'alp = 0.1')
    ax.plot(r, p1, '', label = 'alp = 1.0')

    legend = ax.legend(loc = 'upper center', shadow = True, fontsize = 'x-large')

    # Put a nicer background color on the legend.
    # legend.get_frame().set_facecolor('C0')
    # plt.plot(r, p)
    # plt.plot(r, p1)
    plt.show()

	# app = QApplication(sys.argv)
	# mainFrame = MainFrame1DWOD()
	# mainFrame.showMaximized()
	# sys.exit(app.exec_())
	
if __name__ == '__main__':
	main()

