import math
from cell import Cell
from node import Node
from face import Face

class Grid:
    def __init__(self, nx, ny):
        self.nodes = []
        self.faces = []
        self.cells = []
        self.nx = nx
        self.ny = ny
        self.rw = -1.0
        self.R = -1.0

    def GenerateGrid(self, rw, R):
        self.rw = rw
        self.R = R

        dx = (R - rw) / self.nx
        dy = 2 * math.pi / self.ny

        self.GenerateNodes(dx, dy)
        self.GenerateFaces()
        self.GenerateCells()

    def GenerateNodes(self, dx, dy):
        # generate nodes
        nodeInd = 0
        for j in range(self.ny+1):
            y = 0 + j * dy
            for i in range(self.nx+1):
                x = self.rw + i * dx
                nd = Node(x, y, nodeInd)
                nodeInd += 1
                self.nodes.append(nd)

        # convert to DEcart coordinates;
        needConvert = not False
        if (needConvert):
            for nd in self.nodes:
                r, fi = nd.x, nd.y
                nd.x = r * math.cos(fi)
                nd.y = r * math.sin(fi)


    def GenerateFaces(self):
        self.GenerateHorizontalFaces()
        self.GenerateVerticalFaces()

    def GenerateHorizontalFaces(self):
        m = 0
        for i in range ((self.nx + 1) * self.ny):
            if (m == self.nx):
                m = 0
                continue
            fc = Face(len(self.faces))
            fc.nodes.append(i)
            fc.nodes.append(i+1)
            self.faces.append(fc)
            m += 1

    def GenerateVerticalFaces(self):
        k0 = (self.nx + 1) * (self.ny-1)
        for i in range (k0):
            fc = Face(len(self.faces))
            fc.nodes.append(i)
            fc.nodes.append(i+(self.nx + 1))
            self.faces.append(fc)

        for i in range(self.nx+1):
            fc = Face(len(self.faces))
            fc.nodes.append(i + k0)
            fc.nodes.append(i)
            self.faces.append(fc)

    def GenerateCells(self):
        sm = 0
        m = 0
        for i in range(self.nx * (self.ny-1)):
            cl = Cell(len(self.cells))
            if (m == self.nx):
                m = 0
                sm += 1
            cl.faces.clear()
            cl.faces.append(i)
            cl.faces.append(i + 1 + self.nx * self.ny + sm)
            cl.faces.append(i + self.nx)
            cl.faces.append(i + self.nx * self.ny + sm)

            cl.nodes.clear()
            cl.nodes.append(self.faces[cl.faces[0]].nodes[0])
            cl.nodes.append(self.faces[cl.faces[0]].nodes[1])
            cl.nodes.append(self.faces[cl.faces[2]].nodes[1])
            cl.nodes.append(self.faces[cl.faces[2]].nodes[0])

            self.cells.append(cl)
            m += 1

        k0 = self.nx * (self.ny-1)
        for i in range(self.nx):
            cl = Cell(len(self.cells))
            cl.faces.clear()
            cl.faces.append(i + k0)
            cl.faces.append(i + self.nx * self.ny + (self.nx+1) * (self.ny-1) + 1)
            cl.faces.append(i)
            cl.faces.append(i + self.nx * self.ny + (self.nx+1) * (self.ny-1))

            cl.nodes.clear()
            cl.nodes.append(self.faces[cl.faces[0]].nodes[0])
            cl.nodes.append(self.faces[cl.faces[0]].nodes[1])
            cl.nodes.append(self.faces[cl.faces[2]].nodes[1])
            cl.nodes.append(self.faces[cl.faces[2]].nodes[0])

            self.cells.append(cl)


            
