import node
from node import Node

class Cell:
    def __init__(self, index):
        self.area = 0.0
        self.faces = []
        self.nodes = []
        self.cntr = Node(0.0, 0.0, -1)
        self.ind = index;
			
