import node
from node import Node

class Face:
    def __init__(self, index):
        self.length = 0
        self.nodes = []
        self.cntr = Node(0.0, 0.0, -1)
        self.cl1 = -1
        self.cl2 = -1
        self.ind = index
