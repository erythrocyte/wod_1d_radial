from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom
from ElemInitFile import InitFileElements

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


class InitFileWorker:	
    def __init__(self, mainFrame, els):
        self.mainFrame = mainFrame
        self.els = els

    def SaveInitFile(self, fullName):
        root = Element(self.els.root)

        mesh = SubElement(root, self.els.meshRoot)
        meshN = SubElement(mesh, self.els.meshN)
        meshN.text = str(self.mainFrame.sbN.value())

        bounds = SubElement(root, self.els.boundRoot)
        bound_pw = SubElement(bounds, self.els.boundPw)
        bound_pw.text = str(self.mainFrame.sbPw.value())
        bound_p = SubElement(bounds, self.els.boundPGamma)
        bound_p.text = str(self.mainFrame.sbPg.value())
        bound_s = SubElement(bounds, self.els.boundS)
        bound_s.text = str(self.mainFrame.sbSg.value())
        bound_q = SubElement(bounds, self.els.boundQ)
        bound_q.text = str(self.mainFrame.sbqw.value())

        calcOpt = SubElement(root, self.els.calcOptions)
        edgeFlow = SubElement(calcOpt, self.els.useEdgeFlow)
        edgeFlow.text = 'TRUE' if (self.mainFrame.chUseInflow.isChecked()) else 'FALSE'
        popr = SubElement(calcOpt, self.els.usePopravka)
        popr.text = 'TRUE' if (self.mainFrame.chUseCeff.isChecked()) else 'FALSE'
        flowRateMode = SubElement(calcOpt, self.els.useFlowRateMode)
        flowRateMode.text = 'TRUE' if (self.mainFrame.chGiven_q.isChecked()) else 'FALSE'

        solverOpt = SubElement(root, self.els.solverOptions)
        curNum = SubElement(solverOpt, self.els.sbCurant)
        curNum.text = str(self.mainFrame.sbCurant.value())

        initCond = SubElement(root, self.els.initRoot)
        initSatur = SubElement(initCond, self.els.initSatur)
        initSatur.text = str(0.0)

        saveOpt = SubElement(root, self.els.saveRoot)
        savePressTimeStep =SubElement(saveOpt, self.els.savePressTimeStep)
        savePressTimeStep.text = str(0.0)
        saveSaturTimeStep = SubElement(saveOpt, self.els.saveSaturTimeStep)
        saveSaturTimeStep.text = str(0.0)
        saveObvTimeStep = SubElement(saveOpt, self.els.saveObvTimeStep)
        saveObvTimeStep.text = str(0.0)

        geomOpt = SubElement(root, self.els.geomRoot)
        geomRw = SubElement(geomOpt, self.els.geomRw)
        geomRw.text = str(self.mainFrame.sbrw.value())
        geomR = SubElement(geomOpt, self.els.geomR)
        geomR.text = str(self.mainFrame.sbr.value())

        s = prettify(root)
        f = open(fullName, 'w')
        ss = str(s).strip()
        f.write(ss)
        f.close()

    def ReadInitFile(self, filePath):
        tree = ElementTree.parse(filePath)
        root = tree.getroot()

        mesh = root.find(self.els.meshRoot)
        meshN = mesh.find(self.els.meshN)
        self.mainFrame.sbN.setValue(float(meshN.text))

        bounds = root.find(self.els.boundRoot)
        bound_pw = bounds.find(self.els.boundPw)
        self.mainFrame.sbPw.setValue(float(bound_pw.text))
        bound_p = bounds.find(self.els.boundPGamma)
        self.mainFrame.sbPg.setValue(float(bound_p.text))
        bound_s = bounds.find(self.els.boundS)
        self.mainFrame.sbSg.setValue(float(bound_s.text))
        bound_q = bounds.find(self.els.boundQ)
        self.mainFrame.sbqw.setValue(float(bound_q.text))

        calcOpt = root.find(self.els.calcOptions)
        edgeFlow = calcOpt.find(self.els.useEdgeFlow)
        self.mainFrame.chUseInflow.setChecked(True) if (edgeFlow.text == 'TRUE') else self.mainFrame.chUseInflow.setChecked(False)
        popr = calcOpt.find(self.els.usePopravka)
        self.mainFrame.chUseCeff.setChecked(True) if (popr.text == 'TRUE') else self.mainFrame.chUseCeff.setChecked(False)
        flowRateMode = calcOpt.find(self.els.useFlowRateMode)
        flowRateAnsw = True if (flowRateMode.text == 'TRUE') else False
        self.mainFrame.chGiven_q.setChecked(flowRateAnsw)

        solverOpt = root.find(self.els.solverOptions)
        curNum = solverOpt.find(self.els.sbCurant)

        initCond = root.find(self.els.initRoot)
        initSatur = initCond.find(self.els.initSatur)

        saveOpt = root.find(self.els.saveRoot)
        savePressTimeStep = saveOpt.find(self.els.savePressTimeStep)
        saveSaturTimeStep = saveOpt.find(self.els.saveSaturTimeStep)
        saveObvTimeStep = saveOpt.find(self.els.saveObvTimeStep)

        geomOpt = root.find(self.els.geomRoot)
        geomRw = geomOpt.find(self.els.geomRw)
        self.mainFrame.sbrw.setValue(float(geomRw.text))
        geomR = geomOpt.find(self.els.geomR)
        self.mainFrame.sbr.setValue(float(geomR.text))



