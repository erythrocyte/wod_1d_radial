class InitFileElements:
    def __init__(self):
        self.root = 'WOD_1D_INIT_FILE'

        self.meshRoot = 'MESH'
        self.meshN = 'N'

        self.boundRoot = 'BOUNDS'
        self.boundPw = 'PRESS_WELL'
        self.boundPGamma = 'PRESS_EXTERNAL_BOUND'
        self.boundS = 'BOUND_SATUR'
        self.boundQ = 'BOUND_FLOWRATE'

        self.calcOptions = 'CALC_OPTIONS'
        self.useEdgeFlow = 'USE_EDGE_FLOW'
        self.usePopravka = 'USE_POPRAVKA'
        self.useFlowRateMode = 'USE_FLOWRATE_MODE'
        self.calcDuration = 'CALC_DURATION'

        self.solverOptions = 'SOLVER_OPTION'
        self.sbCurant = 'CURANT_NUMBER'

        self.initRoot = 'INITIAL_CONDITIONS'
        self.initSatur = 'SATUR'


        self.saveRoot = 'SAVE_OPTIONS'
        self.savePressTimeStep = 'SAVE_PRESS_TIME_STEP'
        self.saveSaturTimeStep = 'SAVE_SATUR_TIME_STEP'
        self.saveObvTimeStep = 'SAVE_OBV_TIME_STEP'

        self.geomRoot = 'GEOM_OPTIONS'
        self.geomRw = 'WELL_RADIUS'
        self.geomR = 'BOUND_RADIUS'

