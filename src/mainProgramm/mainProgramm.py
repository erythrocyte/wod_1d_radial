import grid
from grid import Grid


class MainProgram:
    def __init__(self, nx, ny):
        self.grid = Grid(nx, ny)
