
from scipy import special
import scipy.special as sc

def GetAnalyticalPress(alp, rw, R, pw, N):
    ror = alp * R
    row = alp * rw

    dro = (ror - row) / (N-1)

    p = []
    mesh_ro = []
    
    for i in range(N):
        ro = row + dro * i
        print ('mesh[{0}] = {1}'.format(i, ro / alp))
        k1_ror = sc.k1(ror)
        i0_ro = sc.i0(ro)
        i1_ror = sc.i1(ror)
        k0_ro = sc.k0(ro)

        print ('{0}, {1}, {2}, {3}'.format(k1_ror, i0_ro, i1_ror, k0_ro))
        znam = pw * (k1_ror * i0_ro + i1_ror * k0_ro)

        i0_row = sc.i0(row)
        k0_row = sc.k0(row)
        chisl = (i0_row * k1_ror + i1_ror * k0_row)

        p_ro = znam / chisl

        p.append(p_ro)
        mesh_ro.append(ro / alp)

    return [mesh_ro, p]

